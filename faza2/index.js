const express=require('express');
const bodyParser=require('body-parser');
const Sequelize=require('sequelize');
const app=express();
app.use(bodyParser.json());

//initializez conexiunea la baza de date
const sequelize=new Sequelize('c9', 'ioanapaula97', '',
                                                        {
                                                            host:'localhost',
                                                            dialect:'mysql'
                                                        }); //nume bd, user, parola, fisier de configurare
//ma autentific
sequelize.authenticate().then(()=>{
    console.log('DATABASE CONNECTION ESTABLISHED SUCCESSFULLY');
}).catch((err)=>{
    console.log(`Failed to connect to database: ${err}`);
});

//definesc modelul tabelelor
//obs: referinta pt tabela va fi in variabila ContUtilizator, numele tabelei este 'conturi'
const ContUtilizator=sequelize.define('conturi', {
   
    
    username:{
        type:Sequelize.STRING,
        allowNull:false
    }
});

const Colectie=sequelize.define('colectii', {
   
    numeColectie:{
        type:Sequelize.STRING,
        allowNull:false
    },
    
    
});

const Deviation=sequelize.define('deviatii', {
   
    
    titlu:{
        type:Sequelize.STRING,
        allowNull:false
    },
    
    imagine:{
        type:Sequelize.BLOB,
        allowNull:false
    },
    dataPostarii:{
        type:Sequelize.STRING,
        allowNull:false
    }
    
});


const Artist=sequelize.define('artisti', {
   
    username:{
        type:Sequelize.STRING,
        allowNull:false
    }
});


//pun foreign keys
// Colectie.belongsTo(ContUtilizator);
// Colectie.belongsTo(Deviation);
// Deviation.belongsTo(Artist);

Artist.hasMany(Deviation);
Deviation.hasMany(Colectie);
ContUtilizator.hasMany(Colectie);



///creez tabelele
//force:true face drop table if exists
//si prind orice eroare de creare a tabelelor in catch
sequelize.sync({force:true}).then(()=>{
    console.log('TABLES CREATED SUCCESSFULLY!');
}).catch((err)=>{
    console.log(`Failed to create tables: ${err}`);
});


/////metode CRUD

//varianta de la seminar
app.post('/artisti', (req, res) => {
    //const idArtist = req.body.idArtist;
    const username = req.body.username;
    Artist.create({
      //idArtist: idArtist,
      username: username
    })
      .then(artisti => {
          res.status(200).send("Artist created succesfully");
      }, (err) => {
          res.status(500).send(err);
      })
  });


//varianta de la curs
// app.post('/artisti-var2', async (req, res) => {
// 	try{
// 		if (req.query.bulk && req.query.bulk == 'on'){
// 			await Artist.bulkCreate(req.body)
// 			res.status(201).json({message : 'Mai multi artisti au fost adaugati'})
// 		}
// 		else{
// 			await Artist.create(req.body)
// 			res.status(201).json({message : 'Un artist a fost adaugat'})
// 		}
// 	}
// 	catch(e){
// 		console.warn(e)
// 		res.status(500).json({message : 'server error'})
// 	}
// });

app.post('/deviatii', (req, res) =>{
    //const idDeviation = req.body.idDeviation;
    const titlu = req.body.titlu;
    const imagine = req.body.imagine;
    const dataPostarii = req.body.dataPostarii;
    const foreignKey=req.body.artistiId;  ///!! in postman trebuie sa dai NULL sau sa inserezi mai intai un artist in tabela artisti ca sa poti sa dai aici foreign key catre acel artist
    Deviation.create({
        //idDeviation: idDeviation,
        titlu: titlu,
        imagine: imagine,
        dataPostarii: dataPostarii,
        artistiId:foreignKey
    })
    .then(deviatii => {
          res.status(200).send("Deviation created succesfully");
      }, (err) => {
          res.status(500).send(err);
      })
  });


app.post('/colectii', (req, res) => {
    //const idColectie = req.body.idColectie;
    const numeColectie = req.body.numeColectie;
    const foreignKeyCont=req.body.conturiId; ///!! in postman trebuie sa dai NULL sau sa inserezi mai intai un cont in tabela conturi ca sa poti sa dai aici foreign key catre acel cont
    const foreignKeyDeviation=req.body.deviatiiId; ///!! in postman trebuie sa dai NULL sau sa inserezi mai intai un o deviatie in tabela deviatii ca sa poti sa dai aici foreign key catre acea deviatie
    Colectie.create({
      //numeColoana: numeVariabila
      
      //idColectie: idColectie,
      numeColectie: numeColectie,
      conturiId: foreignKeyCont,
      deviatiiId: foreignKeyDeviation
    })
      .then(colectii => {
          res.status(200).send("Colectie created succesfully");
      }, (err) => {
          res.status(500).send(err);
      })
  });
  
  app.post('/conturi', (req, res) => {
    //const idCont = req.body.idCont;
    const username = req.body.username;
    ContUtilizator.create({
      //idCont: idCont,
      username: username
    })
      .then(conturi => {
          res.status(200).send("ContUtilizator created succesfully");
      }, (err) => {
          res.status(500).send(err);
      })
  });

app.get('/get-all-artisti', (req,res) =>{
    Artist.findAll().then((artisti) =>{
        res.status(200).send(artisti);    
    });
});

app.get('/get-all-conturi', (req,res) =>{
    ContUtilizator.findAll().then((conturi) =>{
        res.status(200).send(conturi);    
    });
});

app.get('/get-all-deviatii', (req,res) =>{
    Deviation.findAll().then((deviatii) =>{
        res.status(200).send(deviatii);    
    });
});

app.get('/get-all-colectii', (req,res) =>{
    Colectie.findAll().then((colectii) =>{
        res.status(200).send(colectii);    
    });
});

app.put('/deviatii/:id/update', (req, res) =>{
   const id = req.params.id;
   let updated = false;
   Deviation.forEach((Deviation) =>{
      if(Deviation.id == id){
          updated = true;
          Deviation.titlu = req.body.titlu;
          Deviation.imagine = req.body.imagine;
          Deviation.dataPostarii = req.body.dataPostarii;
          Deviation.artistiId = req.body.artistiId;
      } 
   });
   if(updated){
       res.status(200).send(`Deviation ${id} updated!`);    
   } else {
       res.status(404).send(`Could not find deviation with id ${id}`);
   }
});




app.delete('/conturi/:id/delete', (req, res) =>{
   const i = req.params.id - 1;
   
   if(i < ContUtilizator.length){
       ContUtilizator.splice(i, 1);    
       res.status(200).send('ToDo with id ${i + 1} was deleted');
   } else {
       res.status(404).send('Could not find any item with id ${i + 1}');
   }
   
});


app.listen(8080, ()=>{
    console.log('SERVER STARTED ON PORT 8080');
});